
import React from 'react';
import './App.css';
import axios from "axios";
import Webcam from "react-webcam";

export default class App extends React.Component {

  constructor (props) {

    super(props);
    this.state= {
      selectedFile: null,
      fileUploadedSuccessfully: false,
    }

    this.VideoRef = React.createRef();
    this.PhotoRef = React.createRef();
    
  }

  // getVideo = () => {

  //   navigator.mediaDevices.getUserMedia({
  //     video: {width: 1920 , height: 1080}
  //   }).then(stream => {
  //      let video = this.VideoRef.current;
  //      video.srcObject = stream;
  //      video.play();
  //   }).catch(err => {
  //     console.error(err);
  //   })

  // }

  // componentDidMount ( ) {

  //   this.getVideo();

  // }

  onFileChange  = event => {

    const ZERO = 0;
    this.setState({selectedFile: event.target.files[ZERO]});
    console.log(event.target.files[ZERO]);
    console.log(event.target.files[0]);

  }

  dataURLtoFile = (dataurl, filename) => {
    const arr = dataurl.split(",");
    const mime = arr[0].match(/:(.*?);/)[1];
    const bstr = atob(arr[1]);
    let n = bstr.length;
    const u8arr = new Uint8Array(n);
  
    while (n--) u8arr[n] = bstr.charCodeAt(n);
  
    return new File([u8arr], filename, { type: mime });
  };


  onSubmit = () => {

    const formdata = new FormData();

    formdata.append(
      "file",
      this.state.selectedFile,
    )

    console.log(formdata);

    alert("File has been submited");

    this.setState({
      selectedFile: null,
      fileUploadedSuccessfully: true,
    })

    //api call 
    axios.post("https://efuh4mjo1i.execute-api.ap-south-1.amazonaws.com/upload", formdata).then((res) => {

      console.log(res);

    });
  }

  clickPick = (e) => {

    e.preventDefault();

    var image = new Image();

    const imageSrc = this.VideoRef.current.getScreenshot();

    image.src = imageSrc;

    const formdata = new FormData();

    formdata.append(
      "file",
      this.dataURLtoFile(imageSrc, ),
    )

    axios.post("https://efuh4mjo1i.execute-api.ap-south-1.amazonaws.com/uploadImage", formdata).then((res) => {

      console.log(res); 

       var a = document.getElementById('imgref'); 

       a.onClick = window.open(res.data.result.Location, "_blank");
          
      document.body.append(a);

    });

  }

  render () {

    return <div className="App container-fluid">
        <div className="row justify-content-center">
          <div className="col camcomp">
            <Webcam className="webcam" screenshotFormat="image/jpeg" ref={this.VideoRef}/>
          </div>
        </div>
        <div className="row upload">
          <div className="col"> 
            <button  className="btn btn-warning" onClick={(e) => this.clickPick(e)}>Click a Pic</button>
          </div>
        </div>

        <div className="row upload">
          <div className="col">
            <input className="btn btn-light" type="file" onChange={this.onFileChange}></input>
          </div>
        </div>

        <div className="row upload">
          <div className="col">   
            <button className="btn btn-info" onClick ={this.onSubmit}>Upload</button>
          </div>
        </div>
        
        <div className="imagelocations row" id="imgref">
        </div>
      </div>
  }
}

